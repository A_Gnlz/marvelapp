import { Component, OnInit } from '@angular/core';
import { CharactersService } from '../services/characters.service';
import { MatDialog, PageEvent } from '@angular/material';
import { ErrorDialogComponent } from '../error-dialog/error-dialog.component';
import { FormBuilder, FormGroup } from '@angular/forms';
import { LoadingComponent } from '../loading/loading.component';
import { ComicsService } from '../services/comics.service';
import { StoriesService } from '../services/stories.service';

@Component({
  selector: 'app-characters',
  templateUrl: './characters.component.html',
  styleUrls: ['./characters.component.scss']
})
export class CharactersComponent implements OnInit {

  constructor(private charactersService:CharactersService, private comicsService: ComicsService, private storiesService: StoriesService, private dialog: MatDialog, private formBuilder: FormBuilder) { }

  allCharacters: any;
  charactersForm: FormGroup;
  sortCharacters: string = "name";
  comicsCatalogue: any;
  storiesCatalogue: any;

  //paginator
  paginatorLength: number; //number of registers
  pageSize: number = 20;

  ngOnInit() {
    this.getAllCharacters(this.sortCharacters, 0, this.pageSize);
    this.getComicsCatalogue();
    this.getStoriesCatalogue();
    this.formsBuilder();
  }

  formsBuilder(){
    this.charactersForm = this.formBuilder.group({
      characterName : [""],
      comics: [""],
      stories: [""]
    })
  }

  sortToggle(){
    this.sortCharacters == "name" ? this.sortCharacters = "-name" : this.sortCharacters = "name";
    this.getAllCharacters(this.sortCharacters, 0, this.pageSize, this.charactersForm.controls.characterName.value, this.charactersForm.controls.comics.value, this.charactersForm.controls.stories.value);
  }

  getAllCharacters(sort: string, offset: number, limit: number, name?: string, comics?: number, stories?: number){
    this.dialog.open(LoadingComponent, {
      width: "500px",
      disableClose: true
    })
    this.charactersService.getCharacters(sort, offset, limit, name? name: undefined, comics? comics : undefined, stories? stories: undefined)
        .subscribe(res => {
          //console.log("CHARACTERS",res);
          this.paginatorLength = res.data.total;
          this.allCharacters = res.data.results;
          this.dialog.closeAll();
          setTimeout(() => {
            window.scrollTo(0, 0);
          }, 500);
        }, (err)=>{
          //console.log(err);
          this.dialog.closeAll();
          this.dialog.open(ErrorDialogComponent, {
            width: "400px",
            data: err,
            disableClose: true
          })
        })
  }

  getComicsCatalogue(){
    this.comicsService.getComics("issueNumber", 0, 100)
        .subscribe((res)=>{
          //console.log(this.comicsCatalogue);
          this.comicsCatalogue = res.data.results;
        })
  }

  getStoriesCatalogue(){
    this.storiesService.getStories(0, 100)
        .subscribe((res)=>{
          //console.log(this.storiesCatalogue);
          this.storiesCatalogue = res.data.results;
        })
  }

  search(formData: any){
    this.getAllCharacters(this.sortCharacters, 0, this.pageSize, formData.controls.characterName.value, formData.controls.comics.value, formData.controls.stories.value);
  }

  pageChange(event){
    //console.log(event);
    if(event.previousPageIndex < event.pageIndex){
      this.getAllCharacters(this.sortCharacters, event.pageSize*event.pageIndex, this.pageSize, this.charactersForm.controls.characterName.value, this.charactersForm.controls.comics.value, this.charactersForm.controls.stories.value);
    }else{
      this.getAllCharacters(this.sortCharacters, event.pageSize/event.pageIndex, this.pageSize, this.charactersForm.controls.characterName.value, this.charactersForm.controls.comics.value, this.charactersForm.controls.stories.value);
    }
  }

}
