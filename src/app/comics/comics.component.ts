import { Component, OnInit } from '@angular/core';
import { ComicsService } from '../services/comics.service';
import { MatDialog } from '@angular/material';
import { ErrorDialogComponent } from '../error-dialog/error-dialog.component';
import { FormGroup, FormBuilder } from '@angular/forms';
import { LoadingComponent } from '../loading/loading.component';

@Component({
  selector: 'app-comics',
  templateUrl: './comics.component.html',
  styleUrls: ['./comics.component.scss']
})
export class ComicsComponent implements OnInit {

  constructor(private comicsServices: ComicsService, private dialog: MatDialog, private formBuilder: FormBuilder) { }
  
  allComics: any;
  comicsForm: FormGroup;
  sortComics: string = "issueNumber";
  //paginator
  paginatorLength: number; //number of registers
  pageSize: number = 20;
  formatOptions: Array<string> = ["comic", "magazine", "trade paperback", "hardcover", "digest", "graphic novel", "digital comic", "infinite comic"];
  

  ngOnInit() {
    this.getAllComics(this.sortComics, 0, this.pageSize);
    this.formsBuilder();
  }

  formsBuilder(){
    this.comicsForm = this.formBuilder.group({
      comicTitle : [""],
      format: [""],
      issueNumber: [""]
    })
  }

  sortToggle(){
    this.sortComics == "issueNumber" ? this.sortComics = "-issueNumber" : this.sortComics = "issueNumber";
    this.getAllComics(this.sortComics, 0, this.pageSize, this.comicsForm.controls.format.value, this.comicsForm.controls.comicTitle.value, this.comicsForm.controls.issueNumber.value);
  }
  
  getAllComics(orderBy: string, offset: number, limit: number, format?: string, title?: string, issueNumber?: number){
    this.dialog.open(LoadingComponent, {
      width: "500px",
      disableClose: true
    })
    this.comicsServices.getComics(orderBy, offset, limit, format? format: undefined, title? title: undefined, issueNumber? issueNumber: undefined)
      .subscribe(res =>{
        //console.log("COMICS", res);
        this.paginatorLength = res.data.total;
        this.allComics = res.data.results;
        this.dialog.closeAll();
        setTimeout(() => {
          window.scrollTo(0, 0);
        }, 500);
      }, (err)=>{
        //console.log(err);
        this.dialog.closeAll();
        this.dialog.open(ErrorDialogComponent, {
          width: "400px",
          data: err
        })
      })
  }

  search(formData: any){
    this.getAllComics(this.sortComics, 0, this.pageSize, formData.controls.format.value, formData.controls.comicTitle.value, formData.controls.issueNumber.value);
  }

  pageChange(event){
    //console.log(event);
    if(event.previousPageIndex < event.pageIndex){
      this.getAllComics(this.sortComics, event.pageSize*event.pageIndex, this.pageSize, this.comicsForm.controls.format.value, this.comicsForm.controls.comicTitle.value, this.comicsForm.controls.issueNumber.value);
    }else{
      this.getAllComics(this.sortComics, event.pageSize/event.pageIndex, this.pageSize, this.comicsForm.controls.format.value, this.comicsForm.controls.comicTitle.value, this.comicsForm.controls.issueNumber.value);
    }
    window.scrollTo(0,0);
  }

}
