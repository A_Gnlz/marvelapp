import { Component, OnInit } from '@angular/core';
import { SwUpdate } from '@angular/service-worker';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit{
  title = 'MarvelApp';
  constructor(private swUpdate: SwUpdate){

  }

  ngOnInit(){
    this.swRules();
  }

  swRules(){
    if(this.swUpdate.isEnabled){
      this.swUpdate.activated.subscribe(()=>{
        window.location.reload();
      })
    }
  }
}
