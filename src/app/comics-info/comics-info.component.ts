import { Component, OnInit, Inject } from '@angular/core';
import { MAT_DIALOG_DATA } from '@angular/material';
import { MarvelapiService } from '../marvelapi.service';

@Component({
  selector: 'app-comics-info',
  templateUrl: './comics-info.component.html',
  styleUrls: ['./comics-info.component.scss']
})
export class ComicsInfoComponent implements OnInit {

  constructor(private marvelService: MarvelapiService) {

  }

  ngOnInit() {
    
  }

}
