import { Component, OnInit, Inject } from '@angular/core';
import { MAT_DIALOG_DATA } from '@angular/material';
import { MarvelapiService } from '../marvelapi.service';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-character-info',
  templateUrl: './character-info.component.html',
  styleUrls: ['./character-info.component.scss']
})
export class CharacterInfoComponent implements OnInit {

  constructor(private marvelService: MarvelapiService, public activatedRoute: ActivatedRoute) { 
  }

  ngOnInit() {
  }

}
