import { Component, OnInit } from '@angular/core';
import { StoriesService } from '../services/stories.service';
import { MatDialog } from '@angular/material';
import { ErrorDialogComponent } from '../error-dialog/error-dialog.component';
import { LoadingComponent } from '../loading/loading.component';

@Component({
  selector: 'app-stories',
  templateUrl: './stories.component.html',
  styleUrls: ['./stories.component.scss']
})
export class StoriesComponent implements OnInit {

  allStories: any;
  //paginator
  paginatorLength: number; //number of registers
  pageSize: number = 20

  constructor(private storiesService: StoriesService, private dialog: MatDialog) { }

  ngOnInit() {
    this.getAllStories(0, this.pageSize);
  }

  getAllStories(offset: number, limit: number){
    this.dialog.open(LoadingComponent, {
      width: "500px",
      disableClose: true
    })
    this.storiesService.getStories(offset, limit)
        .subscribe((res)=>{
          //console.log("STORIES", res.data);
          this.paginatorLength = res.data.total;
          this.allStories = res.data.results;
          this.dialog.closeAll();
          setTimeout(() => {
            window.scrollTo(0, 0);
          }, 500);
        }, (err)=>{
          //console.log(err);
          this.dialog.closeAll();
          this.dialog.open(ErrorDialogComponent, {
            width: "400px",
            data: err,
            disableClose: true
          })
        })
  }

  pageChange(event){
    //console.log(event);
    if(event.previousPageIndex < event.pageIndex){
      this.getAllStories(event.pageSize*event.pageIndex, this.pageSize);
    }else{
      this.getAllStories(event.pageSize/event.pageIndex, this.pageSize);
    }
  }

}
