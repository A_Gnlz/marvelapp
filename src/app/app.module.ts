import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { NavbarComponent } from './navbar/navbar.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatToolbarModule, MatGridListModule, MatButtonModule, MatCardModule, MatMenuModule, MatIconModule, MatDialogModule, MatFormFieldModule, MatInputModule, MatOptionModule, MatSelectModule, MatTabsModule, MatPaginatorModule } from '@angular/material';
import { CharactersComponent } from './characters/characters.component';
import { HttpClientModule } from '@angular/common/http';
import { MarvelapiService } from './marvelapi.service';
import { FlexLayoutModule } from '@angular/flex-layout';
import { ComicsComponent } from './comics/comics.component';
import { CharacterInfoComponent } from './character-info/character-info.component';
import { ComicsInfoComponent } from './comics-info/comics-info.component';
import { CharacterComponent } from './character/character.component';
import { ComicComponent } from './comic/comic.component';
import { StoriesComponent } from './stories/stories.component';
import { StoryInfoComponent } from './story-info/story-info.component';
import { StoryComponent } from './story/story.component';
import { ErrorDialogComponent } from './error-dialog/error-dialog.component';
import { CharactersService } from './services/characters.service';
import { ComicsService } from './services/comics.service';
import { StoriesService } from './services/stories.service';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { LoadingComponent } from './loading/loading.component';
import { ScrollingModule } from '@angular/cdk/scrolling';
import { ServiceWorkerModule } from '@angular/service-worker';
import { environment } from '../environments/environment';

@NgModule({
  declarations: [
    AppComponent,
    NavbarComponent,
    CharactersComponent,
    ComicsComponent,
    CharacterInfoComponent,
    ComicsInfoComponent,
    CharacterComponent,
    ComicComponent,
    StoriesComponent,
    StoryInfoComponent,
    StoryComponent,
    ErrorDialogComponent,
    LoadingComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    MatToolbarModule,
    HttpClientModule,
    MatGridListModule,
    MatButtonModule,
    MatCardModule,
    FlexLayoutModule,
    MatMenuModule,
    MatIconModule,
    MatDialogModule,
    MatFormFieldModule,
    MatInputModule,
    MatOptionModule,
    MatSelectModule,
    ReactiveFormsModule,
    FormsModule,
    ScrollingModule,
    MatTabsModule,
    MatPaginatorModule,
    ServiceWorkerModule.register('ngsw-worker.js', { enabled: environment.production })
  ],
  providers: [
    MarvelapiService,
    CharactersService,
    ComicsService,
    StoriesService
  ],
  entryComponents: [
    ErrorDialogComponent,
    LoadingComponent
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
