import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { ComicsService } from '../services/comics.service';
import { MatDialog } from '@angular/material';
import { LoadingComponent } from '../loading/loading.component';

@Component({
  selector: 'app-comic',
  templateUrl: './comic.component.html',
  styleUrls: ['./comic.component.scss']
})
export class ComicComponent implements OnInit {

  comicId: number;
  comicInfo: any;
  comicCharacters: any;
  comicStories: any;
  notFoundMessage: string = "Ultron has destroyed the files that contained more information about this comic";


  constructor(public activatedRoute: ActivatedRoute, private comicsService: ComicsService, private dialog: MatDialog) { }

  ngOnInit() {
    this.comicId = Number(this.activatedRoute.snapshot.paramMap.get("id"));
    this.getComicInfo(this.comicId);
    this.getComicsCharacters(this.comicId);
    this.getComicsStories(this.comicId);
  }

  getComicInfo(id: number){
    this.comicsService.getComicInfo(id)
        .subscribe((res)=>{
          //console.log(res);
          this.comicInfo = res.data.results[0];
        })
  }

  getComicsCharacters(id: number){
    this.dialog.open(LoadingComponent, {
      width: "500px",
      disableClose: true
    })
    this.comicsService.getComicsCharacters(id)
        .subscribe((res)=>{
          //console.log(res);
          this.comicCharacters = res.data.results;
          this.dialog.closeAll();
        })
  }

  getComicsStories(id: number){
    this.comicsService.getComicsStories(id)
        .subscribe((res)=>{
          //console.log(res);
          this.comicStories = res.data.results;
        })
  }

  backNavigation(){
    window.history.back();
  }

}
