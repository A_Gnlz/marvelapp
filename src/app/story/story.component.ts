import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { StoriesService } from '../services/stories.service';

@Component({
  selector: 'app-story',
  templateUrl: './story.component.html',
  styleUrls: ['./story.component.scss']
})
export class StoryComponent implements OnInit {

  storyId: number;
  storyInfo: any;
  storyCharacters: any;
  storyComics: any;
  notFoundMessage: string = "Ultron has destroyed the files that contained more information about this story";


  constructor(public ActivatedRoute: ActivatedRoute, private storiesService: StoriesService) { }

  ngOnInit() {
    this.storyId = Number(this.ActivatedRoute.snapshot.paramMap.get("id"));
    this.getStoryInfo(this.storyId);
    this.getStoryCharacters(this.storyId);
    this.getStoryComics(this.storyId);
  }

  getStoryInfo(id: number){
    this.storiesService.getStoryInfo(id)
        .subscribe((res)=>{
          this.storyInfo = res.data.results[0];
        })
  }

  getStoryCharacters(id: number){
    this.storiesService.getStoryCharacters(id)
        .subscribe((res)=>{
          this.storyCharacters = res.data.results;
        })
  }

  getStoryComics(id: number){
    this.storiesService.getStoryComics(id)
        .subscribe((res)=>{
          this.storyComics = res.data.results;
        })
  }

  backNavigation(){
    window.history.back();
  }

}
