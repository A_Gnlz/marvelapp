import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { CharactersService } from '../services/characters.service';
import { MatDialog } from '@angular/material';
import { ErrorDialogComponent } from '../error-dialog/error-dialog.component';
import { FormGroup, FormBuilder } from '@angular/forms';
import { LoadingComponent } from '../loading/loading.component';

@Component({
  selector: 'app-character',
  templateUrl: './character.component.html',
  styleUrls: ['./character.component.scss']
})
export class CharacterComponent implements OnInit {

  characterId: number;
  characterInfo: any;
  characterComics: any;
  characterStories: any;
  notFoundMessage: string = "Ultron has destroyed the files that contained more information about this character";
  

  constructor(public activatedRoute: ActivatedRoute, private charactersService: CharactersService, private dialog: MatDialog) {

  }

  ngOnInit() {
    this.characterId = Number(this.activatedRoute.snapshot.paramMap.get("id"));
    this.getCharacterInfo(this.characterId);
    this.getCharacterComics(this.characterId);
    this.getCharacterStories(this.characterId);
  }

  getCharacterInfo(id: number){
    this.charactersService.getCharacterInfo(id)
        .subscribe((res)=>{
          //console.log(res);
          this.characterInfo = res.data.results[0];
        }, (err)=>{
          this.dialog.open(ErrorDialogComponent, {
            width: "400px",
            data: err
          })
        })
  }

  getCharacterComics(id: number){
    this.dialog.open(LoadingComponent, {
      width: "500px",
      disableClose: true
    })
    this.charactersService.getCharactersComics(id)
        .subscribe((res)=>{
          //console.log(res);
          this.characterComics = res.data.results;
          this.dialog.closeAll();
        })
  }

  getCharacterStories(id: number){
    this.charactersService.getCharactersStories(id)
        .subscribe((res)=>{
          //console.log(res);
          this.characterStories = res.data.results;
        })
  }

  backNavigation(){
    window.history.back();
  }

}
