import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map} from 'rxjs/operators';
import { MarvelapiService } from '../marvelapi.service';

@Injectable({
  providedIn: 'root'
})
export class ComicsService {

  constructor(private http: HttpClient, private configAPI: MarvelapiService) { }

  getComics(orderBy: string, offset: number, limit: number, format?: string, title?: string, issueNumber?: number): Observable<any>{
    return this.http.get(`${this.configAPI.URL_API}/comics?limit=${limit}&offset=${offset}&orderBy=${orderBy}&${title != undefined? "title="+title+"&" : ""}${format != undefined? "format="+format+"&" : ""}${issueNumber != undefined? "issueNumber="+issueNumber+"&" : ""}${this.configAPI.URL_PARAMS}`, this.configAPI.httpOptions)
            .pipe(map((res)=>res));
  }

  getComicInfo(id: number): Observable<any>{
    return this.http.get(`${this.configAPI.URL_API}/comics/${id}?${this.configAPI.URL_PARAMS}`, this.configAPI.httpOptions)
    .pipe(map((res)=>res));
  }

  getComicsCharacters(id: number): Observable<any>{
    return this.http.get(`${this.configAPI.URL_API}/comics/${id}/characters?${this.configAPI.URL_PARAMS}`, this.configAPI.httpOptions)
            .pipe(map((res)=>res));
  }

  getComicsStories(id: number): Observable<any>{
    return this.http.get(`${this.configAPI.URL_API}/comics/${id}/stories?${this.configAPI.URL_PARAMS}`, this.configAPI.httpOptions)
            .pipe(map((res)=>res));
  }
}
