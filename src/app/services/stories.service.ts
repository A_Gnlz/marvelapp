import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map} from 'rxjs/operators';
import { MarvelapiService } from '../marvelapi.service';

@Injectable({
  providedIn: 'root'
})
export class StoriesService {

  constructor(private http: HttpClient, private configAPI: MarvelapiService) { }

  getStories(offset: number, limit: number): Observable<any>{
    return this.http.get(`${this.configAPI.URL_API}/stories?limit=${limit}&offset=${offset}&${this.configAPI.URL_PARAMS}`, this.configAPI.httpOptions)
            .pipe(map((res)=>res));
  }

  getStoryInfo(id: number): Observable<any>{
    return this.http.get(`${this.configAPI.URL_API}/stories/${id}?&${this.configAPI.URL_PARAMS}`, this.configAPI.httpOptions)
            .pipe(map((res)=>res));
  }

  getStoryCharacters(id: number): Observable<any>{
    return this.http.get(`${this.configAPI.URL_API}/stories/${id}/characters?${this.configAPI.URL_PARAMS}`, this.configAPI.httpOptions)
            .pipe(map((res)=>res));
  }

  getStoryComics(id: number): Observable<any>{
    return this.http.get(`${this.configAPI.URL_API}/stories/${id}/comics?${this.configAPI.URL_PARAMS}`, this.configAPI.httpOptions)
            .pipe(map((res)=>res));
  }

}
