import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map} from 'rxjs/operators';
import { MarvelapiService } from '../marvelapi.service';

@Injectable({
  providedIn: 'root'
})
export class CharactersService {

  constructor(private http: HttpClient, private configAPI: MarvelapiService) { }

  getCharacters(orderBy: string, offset: number, limit: number, name?: string, comics?: number, stories?: number): Observable<any>{
    return this.http.get(`${this.configAPI.URL_API}/characters?limit=${limit}&offset=${offset}&orderBy=${orderBy}&${name != undefined? "name="+name+"&" : ""}${comics != undefined? "comics="+comics+"&" : ""}${stories != undefined? "stories="+stories+"&" : ""}${this.configAPI.URL_PARAMS}`, this.configAPI.httpOptions)
            .pipe(map((res)=>res));
  }

  getCharacterInfo(id: number): Observable<any>{
    return this.http.get(`${this.configAPI.URL_API}/characters/${id}?${this.configAPI.URL_PARAMS}`, this.configAPI.httpOptions)
            .pipe(map((res)=>res));
  }

  getCharactersComics(id: number): Observable<any>{
    return this.http.get(`${this.configAPI.URL_API}/characters/${id}/comics?${this.configAPI.URL_PARAMS}`, this.configAPI.httpOptions)
            .pipe(map((res)=>res));
  }

  getCharactersStories(id: number): Observable<any>{
    return this.http.get(`${this.configAPI.URL_API}/characters/${id}/stories?${this.configAPI.URL_PARAMS}`, this.configAPI.httpOptions)
            .pipe(map((res)=>res));
  }

  
}
