import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})

export class MarvelapiService {
  public httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json'
    })
  }
  public PUBLIC_KEY: string = `b48ab7c7c3506a3f21ed09d1aa9ea2d7`;
  public HASH: string = `cf850fb76ec0f50e77d974a1ee5c1908`;
  public URL_API: string = `https://gateway.marvel.com:443/v1/public`;
  public URL_PARAMS: string = `ts=1&apikey=${this.PUBLIC_KEY}&hash=${this.HASH}`;

  constructor(private http: HttpClient) { }

}
