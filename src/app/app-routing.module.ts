import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CharactersComponent } from './characters/characters.component';
import { ComicsComponent } from './comics/comics.component';
import { CharacterComponent } from './character/character.component';
import { CharacterInfoComponent } from './character-info/character-info.component';
import { ComicsInfoComponent } from './comics-info/comics-info.component';
import { ComicComponent } from './comic/comic.component';
import { StoriesComponent } from './stories/stories.component';
import { StoryInfoComponent } from './story-info/story-info.component';
import { StoryComponent } from './story/story.component';


const routes: Routes = [
  {
    path: "",
    redirectTo: "/comics",
    pathMatch: "full"
  },
  {
    path: "characters",
    component: CharactersComponent,
  },
  {
    path: "character",
    component: CharacterInfoComponent,
    children: [
      {
        path: ":id",
        component: CharacterComponent
      }
    ]
  },
  {
    path: "comics",
    component: ComicsComponent
  },
  {
    path: "comic",
    component: ComicsInfoComponent,
    children: [
      {
        path: ":id",
        component: ComicComponent
      }
    ]
  },
  {
    path: "stories",
    component: StoriesComponent
  },
  {
    path: "story",
    component: StoryInfoComponent,
    children: [
      {
        path: ":id",
        component: StoryComponent
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
