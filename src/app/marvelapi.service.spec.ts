import { TestBed } from '@angular/core/testing';

import { MarvelapiService } from './marvelapi.service';

describe('MarvelapiService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: MarvelapiService = TestBed.get(MarvelapiService);
    expect(service).toBeTruthy();
  });
});
